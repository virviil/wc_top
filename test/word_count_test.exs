defmodule WcTop.WordCountTest do
  use ExUnit.Case
  doctest WcTop.WordCount
  alias WcTop.WordCount

  describe "from_string" do
    test "Parses empty string to empty map" do
      assert WordCount.from_string("") == %{}
    end

    test "parses comma separated in right manner" do
      assert WordCount.from_string("a,b,c") == %{"a" => 1, "b" => 1, "c" => 1}
    end

    test "Parses case sensitive" do
      assert WordCount.from_string("a,A") == %{"a" => 1, "A" => 1}
    end

    test "bypasses whitespaces" do
      assert WordCount.from_string("a, b  ,c") == %{"a" => 1, "b" => 1, "c" => 1}
    end
  end
end
