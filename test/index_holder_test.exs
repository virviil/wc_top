defmodule WcTop.IndexHolderTest do
  use ExUnit.Case, async: false
  doctest WcTop.IndexHolder

  alias WcTop.IndexHolder


  test "gets empty hist" do
    assert IndexHolder.get_hist(5) == []
  end

  test "Writes good hist for random indexed data" do
    IndexHolder.index_chunk(%{"a" => 1})
    IndexHolder.index_chunk(%{"a" => 1, "b" => 4})
    IndexHolder.index_chunk(%{"a" => 1, "b" => 4, "c" => 2})
    IndexHolder.index_chunk(%{"c" => 1, "d" => 500})
    IndexHolder.index_chunk(%{"a" => 1, "c" => 34})

    assert IndexHolder.get_hist(5) == [%{"d" => 500}, %{"c" => 37}, %{"b" => 8}, %{"a" => 4}]

  end
end
