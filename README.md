# WcTop

## Asumptions

1. We are expecting, that data indexing occures more frequently than aggregation query.
1. Data is stored inside RAM. Service restarts remove all previous indexed data.
1. Top-5 elements are retreived using max HEAP data structure.
1. Service exposes full read and write concurrency
1. Words are case-sensitive (it means then "hello" and "Hello" are different words).
1. Single words bulk can have multiple occurences of different words.

## API description

1. Returns histogram of top 5 words ordered by there occurences. Returns at max words, if the number if index words is less then 5.
    
    `GET /`

    * return-type: application/json
    * 200 OK

    

    Example:

    ```
    $ curl -v localhost:4000
    > GET / HTTP/1.1
    > Host: localhost:4000
    > User-Agent: curl/7.77.0
    > Accept: */*
    > 
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 200 OK
    < cache-control: max-age=0, private, must-revalidate
    < content-length: 70
    < content-type: application/json; charset=utf-8
    < server: Cowboy
    < 
    
    [{"hello":15494},{"in":13617},{"word":13364},{"ball":13364},{"neck":12489}]
    ```

2. Gets a string with comma-separated words and index them

    `POST /`

    * accept-type: text/text
    * 202 Accepted


    Example:

    ```
    curl -v -H "Content-Type: text/plain" --data "this is raw data" localhost:4000
    > POST / HTTP/1.1
    > Host: localhost:4000
    > User-Agent: curl/7.77.0
    > Accept: */*
    > Content-Type: text/plain
    > Content-Length: 16
    > 
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 202 Accepted
    < cache-control: max-age=0, private, must-revalidate
    < content-length: 8
    < server: Cowboy
    < 
    ```


## Running

Use docker to run service instance fast:

```
  $ docker build . -t wc_top
  $ docker run -d -p 4000:4000 wc_top
```

Service exposes port 4000.
