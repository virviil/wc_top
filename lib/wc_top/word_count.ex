defmodule WcTop.WordCount do
  @spec from_string(binary) :: map
  def from_string(cs_string) do
    cs_string
    |> String.split(~r{(\\n|[^\w'])+})
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.frequencies()
  end
end
