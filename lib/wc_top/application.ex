defmodule WcTop.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: WcTop.Router,
        options: [port: 4000]
      ),
      WcTop.IndexHolder
    ]

    opts = [strategy: :one_for_one, name: WcTop.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
