defmodule WcTop.Router do
  use Plug.Router

  plug(Plug.Logger, log: :debug)

  plug(Plug.Parsers,
    parsers: [:urlencoded, :multipart],
    pass: ["text/*"]
  )

  plug(:match)
  plug(:dispatch)

  get "/" do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Jason.encode!(WcTop.IndexHolder.get_hist(5)))
  end

  post "/" do
    {:ok, body, conn} = Plug.Conn.read_body(conn)

    body
    |> WcTop.WordCount.from_string()
    |> WcTop.IndexHolder.index_chunk()

    send_resp(conn, 202, "")
  end

  match _ do
    send_resp(conn, 404, "")
  end
end
