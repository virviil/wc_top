defmodule WcTop.IndexHolder do
  use GenServer

  def start_link(arg) do
    GenServer.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    # Creating an index
    :ets.new(:wc_index, [:ordered_set, :public, :named_table, {:write_concurrency, true}])
    {:ok, []}
  end

  @spec index_chunk(%{String.t() => integer()}) :: :ok
  def index_chunk(counter_data) do
    counter_data
    |> Enum.each(fn {word, count} ->
      :ets.update_counter(:wc_index, word, count, {word, 0})
    end)
  end

  @spec get_hist(integer()) :: [%{String.t() => integer()}]
  def get_hist(top_number) do
    :ets.foldl(
      fn {word, count}, heap ->
        Heap.push(heap, {count, word})
      end,
      Heap.max(),
      :wc_index
    )
    |> Stream.unfold(fn heap ->
      case Heap.split(heap) do
        {nil, nil} -> nil
        {root, tail} -> {root, tail}
      end
    end)
    |> Stream.take(top_number)
    |> Enum.map(fn {count, word} ->
      %{word => count}
    end)
  end
end
