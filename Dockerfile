ARG MIX_ENV="prod"

FROM hexpm/elixir:1.13.3-erlang-24.3.2-alpine-3.14.3 AS build

WORKDIR /app
RUN mix local.hex --force && \
    mix local.rebar --force

ARG MIX_ENV
ENV MIX_ENV="${MIX_ENV}"

COPY mix.exs mix.lock ./
RUN mix deps.get --only $MIX_ENV

COPY lib lib
RUN mix compile

RUN mix release

FROM alpine:3.14.2 AS app

RUN apk add --no-cache libstdc++ openssl ncurses-libs

ARG MIX_ENV="prod"

ENV USER="elixir"

WORKDIR "/home/${USER}/app"

RUN \
    addgroup \
    -g 1000 \
    -S "${USER}" \
    && adduser \
    -s /bin/sh \
    -u 1000 \
    -G "${USER}" \
    -h "/home/${USER}" \
    -D "${USER}" \
    && su "${USER}"

USER "${USER}"

COPY --from=build --chown="${USER}":"${USER}" /app/_build/"${MIX_ENV}"/rel/wc_top ./

EXPOSE 4000
ENTRYPOINT ["bin/wc_top"]
CMD ["start"]
