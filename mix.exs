defmodule WcTop.MixProject do
  use Mix.Project

  def project do
    [
      app: :wc_top,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {WcTop.Application, []}
    ]
  end

  defp deps do
    [
      {:plug_cowboy, "~> 2.5"},
      {:heap, "~> 2.0"},
      {:jason, "~> 1.3"}
    ]
  end
end
